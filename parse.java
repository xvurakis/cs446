import java.io.File;
//import java.io.BufferedReader;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
//import java.io.InputStreamReader;

public class parse{
	
	//private static parse()

	public static void main(String[] args){
		File f = null;
		int size = 0;
		//System.out.println(args.length);
		if(args.length >= 1){
			f = new File(args[0]);
		}
		Scanner sc = null;
		try{
			sc = new Scanner(f);
			if(sc.hasNextInt()) size = sc.nextInt();
			else {
				System.out.println("First line of the file has to be an int, that is the size of the arrays!!!");
				System.exit(1);
			}
			double[] inreal = new double[size];
			double[] inimag = new double[size];
			int i = 0;
			while (sc.hasNextLine() && i < size) {
				inreal[i] = sc.nextDouble();
				inimag[i++] = sc.nextDouble();
			}
			sc.close();
			for(i=0;i<inreal.length;i++){
				System.out.println("inreal[" + i + "] = " + inreal[i]);
				System.out.println("inimag[" + i + "] = " + inimag[i]);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}