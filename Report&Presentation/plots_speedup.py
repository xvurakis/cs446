import numpy as np
import matplotlib.pyplot as plt
import sys

labels = ['100', '500', '1000', '2000']
speedup = [9.03, 25.50, 27.07, 55.38]
x = np.arange(len(labels))
fig, ax = plt.subplots()
#width = 0.70
ax.set_ylabel('X times')
ax.set_xlabel('Input size')
#ax.set_yscale('')
ax.set_title('Speedup Comparison')
ax.set_xticks(x)
plt.bar(x, speedup)
ax.set_xticklabels(labels)
ax.legend()




# Create names on the x-axis

 
# Show graphic
plt.show()
