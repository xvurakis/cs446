
Size: 100---------------------------------------------------------------------------------
	GPU Execution: Mean Time = 0.44 ms
	CPU Execution: Mean time = 2.57 ms
	Speedup: x5.840909090909091
	Verification true

Size: 500---------------------------------------------------------------------------------
	GPU Execution: Mean Time = 2.71 ms
	CPU Execution: Mean time = 65.75 ms
	Speedup: x24.2619926199262
	Verification true

Size: 1000---------------------------------------------------------------------------------
	GPU Execution: Mean Time = 9.64 ms
	CPU Execution: Mean time = 262.51 ms
	Speedup: x27.231327800829874
	Verification true

Size: 2000---------------------------------------------------------------------------------
	GPU Execution: Mean Time = 19.15 ms
	CPU Execution: Mean time = 1054.03 ms
	Speedup: x55.040731070496086
	Verification true
