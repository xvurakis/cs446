import numpy as np
import matplotlib.pyplot as plt
import sys

labels = ['100', '500', '1000', '2000']
seq5000 = [2.55, 64.12, 261.78, 1076.7]
par5000 = [0.29, 2.51, 9.5, 19.12]

seq100 = [2.54, 64.21, 257.9, 1063.62]
par100 = [0.41, 2.73, 9.53, 18.92]

speedup5000 = [8.79310345, 25.545816733, 27.55578947, 56.312761506]
speedup100 = [6.195122, 23.5201465, 27.06191, 56.2167019]

x = np.arange(len(labels))
fig, ax = plt.subplots()
width = 0.35
rects1 = ax.bar(x - width/2, speedup5000, width, label='warmup -> 5000 iterations')
rects2 = ax.bar(x + width/2, speedup100, width, label='warmup -> 100 iterations')
ax.set_ylabel('x times')
ax.set_xlabel('Input size')
#ax.set_yscale('log')
ax.set_title('Speedup Comparison of DFT part\nno batch')
#ax.set_title('Speedup Comparison of Quantization part\n(Sequential over Parallel)')
ax.set_xticks(x)
ax.set_xticklabels(labels)
ax.legend()

def autolabel(rects):
    """Attach a text label above each bar in *rects*, displaying its height."""
    for rect in rects:
        height = rect.get_height()
        ax.annotate('{:.3f}'.format(height),
                    xy=(rect.get_x() + rect.get_width() / 2, height),
                    xytext=(0, 3),  # 3 points vertical offset
                    textcoords="offset points",
                    ha='center', va='bottom')


autolabel(rects1)
autolabel(rects2)

fig.tight_layout()

plt.show()

