import java.util.Random;
import java.util.stream.IntStream;
import uk.ac.manchester.tornado.api.TaskSchedule;
import uk.ac.manchester.tornado.api.annotations.Parallel;
import java.util.Vector;
import java.util.*;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import java.io.IOException;
//import uk.ac.manchester.tornado.api.collections.math.TornadoMath;

public final class SimpleReceiver {

	public static long round(double d) {
		if (d > 0) {
			return (long) (d + 0.5d);
		} else {
			return (long) (d - 0.5d);
		}
	}
	
	private static void quantize(double[] inreal, 
								 double[] inimag, 
								 double[] outreal, 
								 double[] outimag, 
								 double minValue, 
								 double maxValue){
		int samples = inreal.length;
		double qStep = (maxValue-minValue)/(2^9-1);
		for (@Parallel int i = 0; i < samples; i++) {
			outreal[i] = minValue + round(((Math.min(Math.max(inreal[i],
													        minValue),
													 		maxValue))-minValue)/qStep)*qStep;
			outimag[i] = minValue + round(((Math.min(Math.max(inimag[i],
															minValue),
															maxValue))-minValue)/qStep)*qStep;
		}
	}

	private static void computeDFT(double[] inreal, 
								double[] inimag, 
								double[] outreal, 
								double[] outimag) {
		int n = inreal.length;
		for (@Parallel int k = 0; k < n; k++) {  // For each output element
			double sumreal = 0;
			double sumimag = 0;
			for (int t = 0; t < n; t++) {  // For each input element
				double angle = 2 * Math.PI * t * k / n; //4
				sumreal +=  inreal[t] * Math.cos(angle) + inimag[t] * Math.sin(angle); //4
				sumimag += -inreal[t] * Math.sin(angle) + inimag[t] * Math.cos(angle);
			}
			outreal[k] = sumreal;
			outimag[k] = sumimag;
		}
	}

	private static boolean verify(double[] parreal, double[] parimag, double[] seqreal, double[] seqimag) {
		boolean check = true;
		for (int i = 0; i < parimag.length; i++) {
			if ( (Math.abs(parreal[i] - seqreal[i]) > 0.1f) || (Math.abs(parimag[i] - seqimag[i]) > 0.1f) ) {
				check = false;
				break;
			}
		}
		return check;
	}

	private static void parse(Scanner sc, int size, double[] inreal, double[] inimag){
		int i = 0;
		while (sc.hasNextLine() && i < size) {
			inreal[i] = sc.nextDouble();
			inimag[i++] = sc.nextDouble();
		}
		sc.close();
	}

	public static void main(String[] args) {
		int size = 100;
		String filename = "";
		File f = null;
		int WARMING_UP_ITERATIONS = 5000;
		int RUN_ITERATIONS = 100;

		if (args.length == 1) {
			try {
				size = Integer.parseInt(args[0]);
			} catch (NumberFormatException ignored) {
				filename = args[0];
			}

		}
		double[] inreal = null;
		double[] inimag = null;

		if(filename != ""){
			f = new File(filename);
			try{
				Scanner sc = new Scanner(f);
				if(sc.hasNextInt()) size = sc.nextInt();
				else {
					System.out.println("First line of the file has to be an int, that is the size of the arrays!!!");
					System.exit(1);
				}
				inreal = new double[size];
				inimag = new double[size];
				parse(sc, size, inreal, inimag);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			inreal = new double[size];
			inimag = new double[size];
			Random r = new Random();
			for(int i = 0;i < size;i++) {
				inreal[i] = r.nextDouble();
				inimag[i] = r.nextDouble();
			}
		}

		System.out.println("Analising " + size + " samples");
		double[] outreal = new double[size];
		double[] outimag = new double[size];
		double[] paroutreal = new double[size];
		double[] paroutimag = new double[size];
		double[] DFToutreal = new double[size];
		double[] DFToutimag = new double[size];
		double[] DFTparoutreal = new double[size];
		double[] DFTparoutimag = new double[size];
		double maxValue = 1;
		double minValue = -1;

		//@formatter:off
		TaskSchedule t = new TaskSchedule("s0")
						.streamIn(inreal)
						.streamIn(inimag)
						.batch("512MB")
						.task("quantize", SimpleReceiver::quantize, inreal, inimag, paroutreal, paroutimag, minValue, maxValue)
						.task("compute_dft", SimpleReceiver::computeDFT, paroutreal, paroutimag, DFTparoutreal, DFTparoutimag)
						.streamOut(DFTparoutreal)
						.streamOut(DFTparoutimag);
		/*TaskSchedule t = new TaskSchedule("s0")
						.streamIn(inreal)
						.streamIn(inimag)
						.batch("512MB")
						.task("quantize", SimpleReceiver::quantize, inreal, inimag, paroutreal, paroutimag, minValue, maxValue)
						//.task("compute_dft", SimpleReceiver::computeDFT, paroutreal, paroutimag, DFTparoutreal, DFTparoutimag)
						.streamOut(paroutreal)
						.streamOut(paroutimag);
						//.streamOut(DFTparoutreal)
						//.streamOut(DFTparoutimag);

		TaskSchedule t2 = new TaskSchedule("s1")
						.streamIn(paroutreal)
						.streamIn(paroutimag)
						.batch("512MB")
						//.task("t0", SimpleReceiver::quantize, inreal, inimag, paroutreal, paroutimag, minValue, maxValue)
						.task("compute_dft", SimpleReceiver::computeDFT, paroutreal, paroutimag, DFTparoutreal, DFTparoutimag)
						.streamOut(DFTparoutreal)
						.streamOut(DFTparoutimag);*/
		//@formatter:on

		// 1. Warm up Tornado
		System.out.print("\nWarming up GPU....");
		for (int i = 0; i < WARMING_UP_ITERATIONS; i++) {
			t.execute();
			//t2.execute();
		}
		System.out.println("....DONE!!!\n");
		
		// 2. Run parallel on the GPU with Tornado
		System.out.println("Analysis running on GPU....");
		double start = System.currentTimeMillis();
		for(int i = 0; i < RUN_ITERATIONS; i++){
			t.execute();
		}
		double end = System.currentTimeMillis();

		/*double start2 = System.currentTimeMillis();
		for(int i = 0; i < RUN_ITERATIONS; i++){
			t2.execute();
		}
		double end2 = System.currentTimeMillis();
		*/
		System.out.println("....DONE!!!\n");

		//Compute Parallel performance
		System.out.println("\tGPU Execution: " + "Mean Time = " + (end - start)/RUN_ITERATIONS + " ms");
		/*System.out.println("\tGPU Execution(quantize): " + "Mean Time = " + (end - start)/RUN_ITERATIONS + " ms");
		System.out.println("\tGPU Execution(dft): " + "Mean Time = " + (end2 - start2)/RUN_ITERATIONS + " ms");*/
		
		// Run sequential
		// 1. Warm up sequential
		System.out.println("\nWarming up CPU....Please wait!!! (MAY TAKE AGES)");
		for (int i = 0; i < WARMING_UP_ITERATIONS; i++) {
			quantize(inreal, inimag, outreal, outimag, minValue, maxValue);
			computeDFT(outreal, outimag, DFToutreal, DFToutimag);
		}
		System.out.println("Warming up CPU....DONE!!!");

		// 2. Run the sequential code
		System.out.println("Analysis running on CPU....Please wait!!! (MAY TAKE CENTURIES)");
		double startSequential = System.currentTimeMillis();
		for(int i = 0; i < RUN_ITERATIONS; i++){
			quantize(inreal, inimag, outreal, outimag, minValue, maxValue);
			computeDFT(outreal, outimag, DFToutreal, DFToutimag);
		}
		double endSequential = System.currentTimeMillis();
		System.out.println("ONE ETERNITY LATER....Analysis on CPU DONE!!!");
		
		/*double startSequential = System.currentTimeMillis();
		for(int i = 0; i < RUN_ITERATIONS; i++){
			quantize(inreal, inimag, outreal, outimag, minValue, maxValue);
			//computeDFT(outreal, outimag, DFToutreal, DFToutimag);
		}
		double endSequential = System.currentTimeMillis();
		double startSequential2 = System.currentTimeMillis();
		for(int i = 0; i < RUN_ITERATIONS; i++){
			//quantize(inreal, inimag, outreal, outimag, minValue, maxValue);
			computeDFT(outreal, outimag, DFToutreal, DFToutimag);
		}
		double endSequential2 = System.currentTimeMillis();
		*/

		//Compute Sequential performance
		double speedup = (endSequential - startSequential) / (end - start);
		//double speedup = (endSequential - startSequential) / (end - start);
		//double speedup2 = (endSequential2 - startSequential2) / (end2 - start2);

		System.out.println("\tCPU Execution: " + "Mean time = " + (endSequential - startSequential)/RUN_ITERATIONS + " ms");
		//System.out.println("\tCPU Execution(quantize): " + "Mean time = " + (endSequential - startSequential)/RUN_ITERATIONS + " ms");
		//System.out.println("\tCPU Execution(dft): " + "Mean time = " + (endSequential2 - startSequential2)/RUN_ITERATIONS + " ms");
		System.out.printf("\tSpeedup: x %.2f\n",speedup);
		//System.out.printf("\tSpeedup: x %.2f\n",speedup + speedup2);
		System.out.println("\tVerification " + verify(DFTparoutreal, DFTparoutimag, DFToutreal, DFToutimag));
		}

}
